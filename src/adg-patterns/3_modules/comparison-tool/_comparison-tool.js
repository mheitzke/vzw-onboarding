/*=======================================
=            COMPARISON TOOL            =
=======================================*/

$('.js-comparison-tool-toggle').click(function(event) {
    $(this).find('.adg-comparison-tool__section__toggle-btn').toggleClass('adg-comparison-tool__section__toggle-btn--open');
    $(this).closest('.adg-comparison-tool__section__header').next('.js-comparison-tool-target').toggleClass('adg-comparison-tool__section__content--open');
}); 

/*=====  End of COMPARISON TOOL  ======*/
