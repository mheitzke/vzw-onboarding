$(document).ready(function() {
    $('.js-select-card').click(function(event) {
        $this = $(this);
        if( $this.hasClass('card-button--selected')){

        }
        else{
            $('.js-select-card').removeClass('card-button--selected');
            $this.addClass('card-button--selected')
            $('.card').removeClass('card--selected')
            $('.js-card-group').addClass('card--muted')
            $this.closest('.js-card-group').addClass('card--selected').removeClass('card--muted')
            $('#js-next-button').removeClass('btn-disabled').removeAttr('disabled')
            $('.btn-caption').addClass('btn-caption--active')
        }
        
    }); 
});