/*================================
=            UPLOADER            =
================================*/

$(document).ready(function() {
	// Dropzone class:
	$(".upload-dropzone").dropzone({ 
		paramName: 'file',
		url: "upload.php",
		clickable: true,
		maxFilesize: 10, 
		addRemoveLinks: true,
		maxFiles:1,
		
	});
	$('.upload-dropzone').on("addedfile", function(file){
		$('body').hide();
	});
});


/*=====  End of UPLOADER  ======*/
