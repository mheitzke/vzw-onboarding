	

/*============================
=            TABS            =
============================*/
	var tabTriggers = '.js-tab'; 

	$(document).on('click', tabTriggers , function(event) {
		event.preventDefault();
		
		var tabTarget = $(this).attr('data-tab');
		var tabContainer = $(this).closest('.js-tabs');
		
		// cycle through sibling tab-triggers/tabs and set them to inactive
		tabContainer.find(tabTriggers).removeClass('tab--active').each(function(index, el) {
			var elTarget = $(el).attr('data-tab');
			$('.js-tab-anchor[data-tab="'+elTarget+'"]').removeClass('tab-anchor--active').addClass('tab-anchor--inactive');
		});
		
		// set clicked tab-trigger/tab to active
		$('.js-tab-anchor[data-tab="'+tabTarget+'"]').addClass('tab-anchor--active').removeClass('tab-anchor--inactive');
		$(this).addClass('tab--active').removeClass('tab--inactive');
		$('.js-select-menu').val(tabTarget);
	});

	$(document).on('change', '.js-select-menu', function(event) {
		event.preventDefault();
		// set clicked tab-trigger/tab to active
		var tabTarget = $(this).val();
		

		$('.js-tab-anchor').removeClass('tab-anchor--active').addClass('tab-anchor--inactive');
		$('.js-tab-anchor[data-tab="'+tabTarget+'"]').addClass('tab-anchor--active').removeClass('tab-anchor--inactive');
		$('.js-tab').removeClass('tab--active').addClass('tab--inactive');
		$('.js-tab[data-tab="'+tabTarget+'"]').addClass('tab--active');
	});


