/*==================================
=            ADG-SEARCH            =
==================================*/


// TYPEAHEAD

var substringMatcher = function(strs) {
    return function findMatches(q, cb) {
        var matches, substrRegex;

        // an array that will be populated with substring matches
        matches = [];

        // regex used to determine if a string contains the substring `q`
        substrRegex = new RegExp(q, 'i');

        // iterate through the pool of strings and for any string that
        // contains the substring `q`, add it to the `matches` array
        $.each(strs, function(i, str) {
            if (substrRegex.test(str)) {
                // the typeahead jQuery plugin expects suggestions to a
                // JavaScript object, refer to typeahead docs for more info
                matches.push({
                    value: str
                });
            }
        });

        cb(matches);
    };
};
var models = [
    'Apple iPhone 3G 8GB',
    'Apple iPhone 4 8GB Black',
    'Apple iPhone 4 16GB Black',
    'Apple iPhone 4 8GB White',
    'Apple iPhone 4 16GB White',
    'Apple iPhone 5 16GB Black',
    'Apple iPhone 5 32GB Black',
    'Apple iPhone 5 64GB Black',
    'Apple iPhone 5 16GB White',
    'Apple iPhone 5 32GB White',
    'Apple iPhone 5 64GB White',
    'Apple iPhone 5S 16GB Black',
    'Apple iPhone 5S 32GB Black',
    'Apple iPhone 5S 64GB Black',
    'Apple iPhone 5S 16GB White',
    'Apple iPhone 5S 32GB White',
    'Apple iPhone 5S 64GB White',
    'Apple iPhone 6 16GB Black',
    'Apple iPhone 6 32GB Black',
    'Apple iPhone 6 64GB Black',
    'Apple iPhone 6 16GB Silver',
    'Apple iPhone 6 32GB Silver',
    'Apple iPhone 6S 64GB Silver',
    'Apple iPhone 6S 16GB Black',
    'Apple iPhone 6S 32GB Black',
    'Apple iPhone 6S 64GB Black',
    'Apple iPhone 6S 16GB Silver',
    'Apple iPhone 6S 32GB Silver',
    'Apple iPhone 6S 64GB Silver',
    'Apple iPhone 7 16GB Black',
    'Apple iPhone 7 32GB Black',
    'Apple iPhone 7 64GB Black',
    'Apple iPhone 7 16GB Silver',
    'Apple iPhone 7 32GB Silver',
    'Apple iPhone 7 64GB Silver',
    'Apple iPhone 7 16GB Gold',
    'Apple iPhone 7 32GB Gold',
    'Apple iPhone 7 64GB Gold',
    'Apple iPhone 7 16GB Rose',
    'Apple iPhone 7 32GB Rose',
    'Apple iPhone 7 64GB Rose',
    
];

$('.js-adg-search-input').typeahead({
    hint: true,
    highlight: true,
    minLength: 0,
    classNames: {
        wrapper: 'adg-search__typeahead',
        input: 'adg-search__typeahead__input',
        hint: 'adg-search__typeahead__hint',
        selectable: 'adg-search__typeahead__selectable',
        menu: 'adg-search__typeahead__menu',
        dataset: 'adg-search__typeahead__dataset',
        suggestion: 'adg-search__typeahead__suggestion',
        empty: 'adg-search__typeahead__empty',
        open: 'adg-serach__open',
        cursor: 'adg-search__typeahead__cursor',
        highlight: 'adg-search__typeahead__highlight'
    }
}, {
    name: 'models',
    displayKey: 'value',
    limit:1000,
    source: substringMatcher(models),
    templates: {
       
        header: 
        	function(data){
        		return [
        		'<div class="adg-search__typeahead__dataset__header">',
		        '<h4 class="adg-search__typeahead__dataset__header__title">Select your device.</h4>',
		        '</div>'].join('\n')

        	},
        notFound:
        	function(data){
        		return [
        		'<div class="adg-search__typeahead__dataset__header">',
		        '<h4 class="adg-search__typeahead__dataset__header__title">No results. Please try again.</h4>',
		        '</div>',
		        '<div class="adg-search__typeahead__dataset__content">',
		        '<p>We didn\'t find any devices matching your search. Try typing the <span class="loud">brand</span>, <span class="loud">model</span>, <span class="loud">storage size</span> and <span class="loud">color</span> of your device.</p><p class="italic">Example: <span>Apple iPhone 6s 32GB Black,</span></p> </p>',
		        '</div>'].join('\n')

        	},
    }
});

$('.js-adg-search-input').on('typeahead:render', function(event) {
	$( ".adg-search__typeahead__suggestion" ).wrapAll( "<div class='adg-search__typeahead__suggestion__wrapper' />");
	if( $('.adg-search__typeahead__suggestion').length > 60 ){
		
		
	}

	if( $( ".adg-search__typeahead__suggestion" ).length >= 5){
		$('.adg-search__typeahead__suggestion__wrapper').addClass('adg-search__typeahead__suggestion__wrapper--fixed-height')

	}
	else{
		$('.adg-search__typeahead__suggestion__wrapper').removeClass('adg-search__typeahead__suggestion__wrapper--fixed-height')	
	}
	
});

$('.js-adg-search-input').on('keyup', function(event) {
	if( $(this).val().length >= 5 && $('.adg-search__typeahead__suggestion').length > 1 ){
		$('.js-search-button').removeClass('btn-disabled').removeClass('btn-ghost').attr({
				disabled: false	
			});
		
	}
	else{
		$('.js-search-button').addClass('btn-disabled').addClass('btn-ghost').attr({
			disabled: true
		});
        $('.js-search-results, .js-search-results-single').hide();
	}
});
$('.js-adg-search-input').on('focus, typeahead:render', function(event) {
	if( $(this).val().length == 0) { 
		$('.adg-search__typeahead__dataset').html('<div class="adg-search__typeahead__dataset__header"><h4 class="adg-search__typeahead__dataset__header__title">Narrow your search</h4></div> <div class="adg-search__typeahead__dataset__content"><div class=""<p>Type the <span class="loud">brand</span>, <span class="loud">model</span>, <span class="loud">storage size</span> and <span class="loud">color</span> of your device.</p><p class="italic">Example: <span>Apple iPhone 6s 32GB Black,</span></p></div>')
	/* Act on the event */
	}
});

$('.js-adg-search-input').on('typeahead:select', function(event) {
    event.preventDefault();
    $('.js-search-results-single').show();
    $('.js-search-results').hide();
    $('#js-next-button').removeClass('btn-disabled').attr({
        disabled: false
    });
    $('.js-adg-search-input').blur();
});


$('.js-search-results, .js-search-results-single').hide();
$('.js-search-form').submit(function(event) {
  event.preventDefault();
  $('.js-search-results').show();
  $('.js-adg-search-input').typeahead('close');
});

$(document).on('click', '.js-select-card', function(event) {
    $this = $(this);
    if( $this.hasClass('card-button--selected')){

    }
    else{
        $('.js-select-card').removeClass('card-button--selected');
        $this.addClass('card-button--selected')
        $('.card').removeClass('card--selected')
        $('.js-card-group').addClass('card--muted')
        $this.closest('.js-card-group').addClass('card--selected').removeClass('card--muted')
        $('#js-next-button').removeClass('btn-disabled').removeAttr('disabled')
        $('.btn-caption').addClass('btn-caption--active')
    }
    
});  
$('.adg-search-results__showMore').click(function(event) {
    event.preventDefault();
    $('.js-content-a').clone().appendTo('.js-content-a')
    $('.js-select-card').removeClass('card-button--selected');
});
/*=====  End of ADG-SEARCH  ======*/
