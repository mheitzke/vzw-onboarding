$(document).ready(function() {
    $('.js-datepicker').datepicker({
        viewMode: "0",
        minViewMode: "0",
        endDate: '+0d'
    });

    $('.card-radio').on('change', function(event) {
        
        var thisArg= $(this).closest('.js-card-radio-group')
            argName = $(this).attr('name')
            arg = $( '[name='+argName+']')

    	if( $(this).is(':checked') ){
            $( '[name='+argName+']').closest('.js-card-radio-group')
                .addClass('card-radio-group--muted')
                .find('input:not(.card-radio)')
                .attr({
                disabled: true
            });
            $(this).closest('.js-card-radio-group')
                .removeClass('card-radio-group--muted')
                .find('input:not(.card-radio)')
                .attr({
                disabled: false
            });
    	}
        
    	
    });

  
});
