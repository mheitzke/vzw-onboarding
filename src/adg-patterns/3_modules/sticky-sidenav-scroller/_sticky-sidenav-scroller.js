/*======================================
=            STICKY SIDENAV            =
======================================*/

function stickySideBar(sidebar, anchors) {
    var sidebar = $(sidebar);
    var anchors = $(anchors);
    if (sidebar.length !== 0) {
        var sticky = new Waypoint.Sticky({
            element: sidebar[0],
            stuckClass: 'js-sticky-sidebar--stuck',
            offset: 0,
            handler: function() {
                if (sidebar.hasClass(this.options.stuckClass)) {
                    var wrapperW = $(".sticky-wrapper").outerWidth();
                    sidebar.css({
                        'width': wrapperW
                    })
                } else {
                    sidebar.attr('style', '');
                }
            }
        });
        anchors.each(function(index, el) {
            var waypoint = new Waypoint({
                element: el,
                offset: 0,
                handler: function(direction) {
                    var anchorId = this.element.id;
                    if (direction === 'down') {
                        $('.sidebar-nav__item--active').removeClass('sidebar-nav__item--active')
                        $('[href="#' + anchorId + '"]').parent().addClass('sidebar-nav__item--active')
                    } else {
                        $('[href="#' + anchorId + '"]').parent().removeClass('sidebar-nav__item--active')
                        $('[href="#' + anchorId + '"]').parent().prev().addClass('sidebar-nav__item--active')
                    }
                }
            })
        });
        if ($(window).width() <= 678) {
            sticky.destroy();
        }
    }
}

$(window).resize(function(event) {
    stickySideBar('.js-sticky-sidebar', '.js-inview-anchor');
}).resize();




/*==========================================
=            ANCHOR LINK SCROLL            =
==========================================*/

function getRelatedContent(el) {
    return $($(el).attr('href'));
}
$('.js-anchor-link').click(function(event) {
    event.preventDefault();
    $('body').animate({ scrollTop: getRelatedContent(this).offset().top - 100 })
});
