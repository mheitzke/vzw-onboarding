$(document).ready(function() {
    
    $('.tooltip-feature').tooltipster({
    	theme: 'tooltipster-feature',
    	trigger: 'click'
    });
    $('[data-tooltip-content]').tooltipster({
    	theme: 'tooltipster-light',
    	maxWidth: 250
    });
});