jQuery(document).ready(function($) {

    $('[data-modal]').click(function(event) {
    	var target=$(this).attr('data-modal'),
    		target= $(target);

    	if( target.hasClass('modal--active') ){
    		target.removeClass('modal--active')
    		$('.modal-overlay').removeClass('modal-overlay--active')
            $('.page').removeClass('blurred');
    	}
    	else{
    		target.addClass('modal--active')
    		$('.modal-overlay').addClass('modal-overlay--active')
            $('.page').addClass('blurred');
    	}
    });
    $('[data-modal-close]').click(function(event) {
    	$(this).closest('.modal').removeClass('modal--active')
    	$('.modal-overlay').removeClass('modal-overlay--active')
        $('.page').removeClass('blurred');
    });
    $('.modal-overlay').click(function(event) {
    	$('.modal--active').removeClass('modal--active')
    	$('.modal-overlay').removeClass('modal-overlay--active')
        $('.page').removeClass('blurred');
    });


    /*==============================
    =            LOADER            =
    ==============================*/
    $('body').on('click', '[data-toggle="loader"], .loader-overlay--active', function(event) {
        event.preventDefault();
        $('.generic-loader').toggleClass('generic-loader--active');
        $('.loader-overlay').toggleClass('loader-overlay--active');
        $('.page').toggleClass('blurred');
    });

   /*============================================
   =            JQUERY INPUT MASKING            =
   ============================================*/
   $('.js-phone_us').mask('(000) 000-0000');


   /*=====  End of JQUERY INPUT MASKING  ======*/

   /*==============================
   =            TOGGLE            =
   ==============================*/
   (function(){
     $('[data-hr-toggle]').each(function(event) {
        var target=$(this).attr('data-hr-toggle'),
            target= $(target);
            target.hide();
     });

     $('[data-hr-toggle]').click(function(event) {
         var target=$(this).attr('data-hr-toggle'),
             target= $(target),
             $this=$(this);

         if( target.is(':hidden') ){
             $this.addClass('hr-toggler__btn--active')
             $this.find('.js-hr-toggler__text-toggle').text('Hide');
             target.slideDown()
         }
         else{
             $this.removeClass('hr-toggler__btn--active')
             target.slideUp();
             $this.find('.js-hr-toggler__text-toggle').text('Show');
         }
     });
   })();


   /*=====================================
   =            CALLOUT CLOSE            =
   =====================================*/


   $('[data-callout-close]').click(function(event) {
    $(this).closest('.callout').slideUp('fast');

   });

   /*=====================================
   =            ALERT CLOSE            =
   =====================================*/


   $('[data-alert-close]').click(function(event) {
    $(this).closest('.alert-box').slideUp('fast');

   });




});
