var gulp = require('gulp'),
    sass = require('gulp-sass'),
    browserSync = require('browser-sync'),
    autoprefixer = require('gulp-autoprefixer'),
    uglify = require('gulp-uglify'),
    jshint = require('gulp-jshint'),
    header  = require('gulp-header'),
    rename = require('gulp-rename'),
    minifyCSS = require('gulp-clean-css'),
    nunjucksRender = require('gulp-nunjucks-render'),
    sourcemaps = require('gulp-sourcemaps'),
    path = require("path"),
    del = require('del'),
    concat = require('gulp-concat'),
    // order = require('gulp-order'),
    addsrc = require('gulp-add-src'),
    package = require('./package.json');

var paths = {
  src:'./src/',
  templates: './src/templates/',
  views: './src/views/**/',
  partials: './src/templates/partials/',
  sass: './src/adg-patterns/',
  theme: './src/adg-patterns/2_theme/',
  js: './src/js/',
  img: './src/adg-patterns/img/',
  dist: './',
  start: ''
}

// css
gulp.task('themeCSS', function () {
    return gulp.src(path.join(paths.theme, '**/*.scss'))
    
    .pipe(sourcemaps.init({debug:true}))
    .pipe(sass({errLogToConsole: true}))
    .pipe(autoprefixer({
          browsers: ['last 2 versions']
        }))
    //.pipe(sourcemaps.write('dist/assets/theme/'+path.dirname)) 
    .pipe(rename(function(path) {
        //path.dirname = 'dist/assets/theme/'+path.dirname
    }))
    // .pipe(minifyCSS({compatibility: 'ie8'}))
    .pipe(gulp.dest('./assets/theme/'))
    .pipe(browserSync.reload({stream:true, once: true}));
});

gulp.task('html', function() {
  nunjucksRender.nunjucks.configure(paths.templates, {
    watch: false,
    tags: {
      variableStart:'{[',
      variableEnd:']}'
    }
  })  
  return gulp.src(paths.views+'*.+(html|nunjucks)')
  .pipe(nunjucksRender())
  .pipe(gulp.dest( paths.dist+'views'))
  .pipe(browserSync.reload({stream:true}));
});

gulp.task('index', function() {
  nunjucksRender.nunjucks.configure(paths.templates, {
    watch: false,
    tags: {
      variableStart:'{[',
      variableEnd:']}'
    }
  })  
    return gulp.src(path.join(paths.src+'*.+(html|nunjucks)'))
    .pipe(nunjucksRender())
    .pipe(gulp.dest(paths.dist))
    .pipe(browserSync.reload({stream:true}));
});


gulp.task('img', function() {
    return gulp.src(path.join(paths.img, '*.+(png|jpg|gif|svg)'))
    .pipe(gulp.dest( paths.dist+'assets/img'))
    .pipe(browserSync.reload({stream:true}));
});

// Theme Image
gulp.task('themeImg', function() {
    return gulp.src(path.join(paths.theme, '**/img/*.*'))
    .pipe(rename(function(path) {
        path.dirname = paths.dist+'assets/theme/'+path.dirname;
    }))
    .pipe(gulp.dest('.'))
    .pipe(browserSync.reload({stream:true}));
});

gulp.task('js',function(){
  return gulp.src(path.join(paths.js, '**/*.js'))
    // .pipe(jshint())
    // .pipe(jshint.reporter('default'))
    // .pipe(order([
    //         'vendor/jquery-1.11.1.min.js',
    //         'app.js',
    //         'formController.js',
    //     ]))
    // .pipe(concat('app.js'))
    // .pipe(uglify({mangle: false}))
    .pipe(gulp.dest( paths.dist+'assets/js'))
    .pipe(browserSync.reload({stream:true, once: true}));
});

gulp.task('modjs', function(){
    return gulp.src(path.join(paths.sass, '3_modules/**/*.js'))
      .pipe(concat('modules.js'))
      .pipe(gulp.dest( paths.dist+'assets/js'))
      .pipe(browserSync.reload({stream:true, once: true}));
});

// Theme Fonts
gulp.task('themeFonts', function() {
    return gulp.src(path.join(paths.theme, '**/fonts/*.*'))
    .pipe(rename(function(path) {
        path.dirname = paths.dist+'assets/theme/'+path.dirname;
    }))
    .pipe(gulp.dest('.'))
    .pipe(browserSync.reload({stream:true}));
});

gulp.task('browser-sync', function() {
    browserSync.init(null, {
        server: {
            baseDir: paths.dist,
            index: paths.start
        }
    });
});
gulp.task('bs-reload', function () {
    browserSync.reload();
});

gulp.task('clean', function() {
  del('dist');
})

gulp.task('misc',  function() {
  gulp.src("src/misc/**/**.*")
      .pipe(gulp.dest('./misc'));
});

gulp.task('default', ['index','themeCSS', 'js', 'img', 'browser-sync', 'html', 'themeFonts', 'themeImg','modjs', 'misc'], function () {
    gulp.watch(path.join(paths.sass, '**/*.scss'), ['themeCSS']);
    gulp.watch(path.join(paths.sass, '**/*.js'), ['modjs']);
    gulp.watch(path.join(paths.js, '**/*.js'), ['js']);
    gulp.watch(path.join(paths.img, '**/*.*'), ['img']);
    gulp.watch(path.join(paths.templates, '*.html'), ['html']);
    gulp.watch(path.join(paths.views, '*.html'), ['html']);
    gulp.watch(path.join(paths.partials, '*.html'), ['html']);
    gulp.watch(path.join(paths.src, '*.html'), ['index']);
});