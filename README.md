++++++++++++++++++++++++++++++
Instructions for collaborators
++++++++++++++++++++++++++++++

1. Install [Node.js](http://nodejs.org/download), [Sass](http://sass-lang.com/tutorial.html) and [Git](http://git-scm.com) on your machine. If you're a Windows user you'll also need to install [Ruby](http://rubyinstaller.org/downloads).
2. [Install Gulp](http://Gulpjs.com/) using `npm install -g gulp`. You may need to use `sudo` in front of the Gulp install command to give it permissions.
3. Open Terminal and install FastShell's dependencies to `node_modules` directory in your project directory using `npm install`. You don't need `sudo` to do this.
4. The `npm install` you did in previous step should install all the dependencies, which you can confirm by visiting the `node_modules` in your project directory. Then use `gulp` (again in your project directory) to run the commands associated with FastShell and to automatically open a new FastShell project running on `localhost:3002`.
5. From now on, just run `gulp` in your project directory to automatically run FastShell's Gulp tasks.


++++++++++++++++++++++++++++++
Notes
++++++++++++++++++++++++++++++

* Git is not tracking the `dist` directory in order to avoid conflicts—it's only tracking `src` files. Be sure to compile these on your local machine after pulling from Git by running `gulp in the terminal in the project directory.
* The `src/adg-patterns` directory contains all the SCSS and image files for all themes and is compiled to the `dist/assets` directory.   The SASS/SCSS is organized into `base`, `theme`, `modules`, and `layouts` directories.
* If this project uses front-end templating, templates are located in `src/templates` and all html screens are located in `src/views`.